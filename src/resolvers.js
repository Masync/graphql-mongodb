import User from "./models/User";
export const resolv = {
    Query: {
        getUsers: async()=> {
           return await User.find();
        },
        getUser: async(_,{id}) => {
           return await User.findById(id)
        }
    },
    Mutation: {
        postUsers:async(_,{nickname, fullname, phone, city}) => {
            
            const user = new User({nickname,fullname,phone,city})
            console.log(user);
            
            return await user.save();
        },
        deleteUser:async(_,{id}) => {
            return await User.findByIdAndDelete(id);
        }
    }

}