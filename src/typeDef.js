export const typeDefinitions = `

    type Query {
        getUser(id: ID!): User
        getUsers: [User]!
    }

    type Mutation {
        postUsers(nickname:String!, fullname: String!, phone: String, city: String): User!
        deleteUser(id:ID!): User
    }
        type User{
            _id: ID!
            nickname: String!,
            fullname: String!,
            phone: String,
            city: String
        }
`