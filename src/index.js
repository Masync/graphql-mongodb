import {GraphQLServer} from 'graphql-yoga'
import { typeDefinitions } from './typeDef'
import { resolv } from './resolvers'
import './database'
const server = new GraphQLServer({
   
    typeDefs: typeDefinitions,
    resolvers: resolv
});

server.start(() => {
    console.log(' Server ok');
});

